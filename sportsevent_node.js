const tableBuilder = require("../../sql_table_builder/builder");

module.exports = async (session, db) => {
  try {
  const jockey_cols = {
    columns: [
      {
        label: "jockeyID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "name", type: "VARCHAR(250)" },
      { label: "url", type: "VARCHAR(100)" },
      { label: "lastUpdated", type: "INT" },
    ],
    primary_key: "PRIMARY KEY (jockeyID)",
  };

  const track_cols = {
    columns: [
      {
        label: "courseID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "name", type: "VARCHAR(50)" },
      { label: "lastUpdated", type: "INT" },
    ],
    primary_key: "PRIMARY KEY (courseID)",
  };

  const horse_cols = {
    columns: [
      {
        label: "horseID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "url", type: "VARCHAR(100)" },
      { label: "name", type: "VARCHAR(250)" },
      { label: "age", type: "INT" },
      { label: "weight", type: "VARCHAR(25)" },
      { label: "lastUpdated", type: "INT" },
      { label: "form", type: "VARCHAR(25)" },
      { label: "runningPosition", type: "VARCHAR(25)" },
    ],
    primary_key: "PRIMARY KEY (horseID)",
  };

  const teams_cols = {
    columns: [
      {
        label: "teamID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "name", type: "VARCHAR(250)" },
      { label: "sport", type: "VARCHAR(100)" },
      { label: "url", type: "VARCHAR(200)" },
      { label: "lastUpdated", type: "INT" },
      { label: "region", type: "VARCHAR(100)" },
    ],
    primary_key: "PRIMARY KEY (teamID)",
  };

  const competitions_cols = {
    columns: [
      {
        label: "competitionID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "name", type: "VARCHAR(100)" },
      { label: "sport", type: "VARCHAR(100)" },
      { label: "lastUpdated", type: "INT" },
      { label: "region", type: "VARCHAR(100)" },
      { label: "url", type: "VARCHAR(200)" },
    ],
    primary_key: "PRIMARY KEY (competitionID)",
  };

  const trainer_cols = {
    columns: [
      {
        label: "trainerID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "name", type: "VARCHAR(250)" },
      { label: "url", type: "VARCHAR(100)" },
      { label: "lastUpdated", type: "INT" },
    ],
    primary_key: "PRIMARY KEY (trainerID)",
  };

  const greyhound_track_cols = {
    columns: [
      {
        label: "trackID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "name", type: "VARCHAR(50)" },
      { label: "lastUpdated", type: "INT" },
    ],
    primary_key: "PRIMARY KEY (trackID)",
  };

  const greyhound_cols = {
    columns: [
      {
        label: "greyhoundID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "url", type: "VARCHAR(100)" },
      { label: "name", type: "VARCHAR(250)" },
      { label: "lastUpdated", type: "INT" },
      { label: "form", type: "VARCHAR(25)" },
    ],
    primary_key: "PRIMARY KEY (greyhoundID)",
  };

  await tableBuilder(jockey_cols, "in4freedom", "jockeys", session);
  await tableBuilder(track_cols, "in4freedom", "horse_tracks", session);
  await tableBuilder(horse_cols, "in4freedom", "horses", session);
  await tableBuilder(teams_cols, "in4freedom", "teams", session);
  await tableBuilder(competitions_cols, "in4freedom", "competitions", session);

  await tableBuilder(trainer_cols, "in4freedom", "trainers", session);
  await tableBuilder(
    greyhound_track_cols,
    "in4freedom",
    "greyhound_tracks",
    session
  );
  await tableBuilder(greyhound_cols, "in4freedom", "greyhounds", session);

  /* {
    url:String
      starttime: int,
      runners: [{
        horseID: int,
        jockeyID: int,
        position: int,
        gate: int,
        equipment: string
        CDWinner: Boolean
      }],
      course: {
        courseID: int!
        distance : Float
      raceType: String
      ground: String
      courseType: String
      }

    } */
  await db.createCollection("horse_races", {
    reuseExisting: true,
  });
  await db.createCollection("sports_events", {
    reuseExisting: true,
  });
  await db.createCollection("esports_events", {
    reuseExisting: true,
  });
  await db.createCollection("greyhound_races", {
    reuseExisting: true,
  });
} catch (err) {
  console.log(err)
  upload_error({
    errorTitle: "Creating Sports Events DB Entities",
    machine: IP.address(),
    machineName: "API",
    errorFileName: __filename.slice(__dirname.length + 1),
    err: err,
    critical: true
  })
}

};
